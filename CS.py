import matplotlib
matplotlib.use('Agg')
import numpy as np #Library for working with arrays,algebra and matrices  
import pandas as pd #Library used for data manipulation and key data structure is called a dataframe(df)
import matplotlib.pyplot as plt#Library used for data visualization(graphs,scatter plot,pie chart)
d = pd.read_csv('https://bitbucket.org/Tom707/titanic/downloads/Mall_Customers.csv')
# Feature selection for the model
#Considering only 2 features (Annual income and Spending Score)
X= d.iloc[:, [3,4]].values
#Building the model
#Using K-Means Algorithm to decide optimum cluster numberfrom sklearn.cluster import KMeans
wcss=[]
from sklearn.cluster import KMeans #sklearn(Sci-kit is a machine learning library)
wcss=[] #Used for calculating within-cluster-sum-of-squares
for i in range(1,11):
    kmeans = KMeans(n_clusters= i, init='k-means++', random_state=0) 
    kmeans.fit(X) #Computing K-Means Clustering
    wcss.append(kmeans.inertia_)
plt.plot(range(1,11), wcss)
plt.title('The Elbow Method')
plt.xlabel('no of clusters')
plt.ylabel('wcss')
plt.show()
kmeansmodel = KMeans(n_clusters= 5, init='k-means++', random_state=0)
y_kmeans= kmeansmodel.fit_predict(X)
plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1], s = 100, c = 'orange', label = 'Cluster 1')
plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1], s = 100, c = 'blue', label = 'Cluster 2')
plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1], s = 100, c = 'green', label = 'Cluster 3')
plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s = 100, c = 'cyan', label = 'Cluster 4')
plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = 'red', label = 'Cluster 5')
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s = 200, c = 'black', label = 'Centroids')
plt.title('Clusters of customers')
plt.xlabel('Annual Income (k$)')
plt.ylabel('Spending Score (1-100)')
plt.savefig('shashank.png')
plt.legend()
#Model Interpretation
#Cluster 1 (Orange Color) -> Average in terms of earning and spending
#cluster 2 (Blue Color) -> Earning less,but spending high
#cluster 3 (Green Color) -> earning high and also spending high [TARGET SET]
#cluster 4 (Cyan Color) -> earning less and spending less
#Cluster 5 (Red Color) -> Earning high,but spending less